<!ENTITY torsettings.dialog.title "Tor tīkla iestatījumi">
<!ENTITY torsettings.wizard.title.default "Veidot savienojumu ar Tor">
<!ENTITY torsettings.wizard.title.configure "Tor tīkla iestatījumi">
<!ENTITY torsettings.wizard.title.connecting "Veido savienojumu">

<!-- For locale picker: -->
<!ENTITY torlauncher.localePicker.title "Pārlūka Tor valoda">
<!ENTITY torlauncher.localePicker.prompt "Lūdzu izvēlieties valodu.">

<!-- For "first run" wizard: -->

<!ENTITY torSettings.connectPrompt "Klikšķināt „Savienot”, lai izveidotu savienojumu ar Tor.">
<!ENTITY torSettings.configurePrompt "Klikšķināt „Konfigurēt”, lai pielāgotu tīkla iestatījumus, ja esat valstīs, kas cenzē Tor (tādās, kā Ēģipte, Ķīna, Turcija), vai veidojat savienojumu no privāta tīkla, kam nepeciešams starpniekserveris.">
<!ENTITY torSettings.configure "Konfigurēt">
<!ENTITY torSettings.connect "Veidot savienojumu">

<!-- Other: -->

<!ENTITY torsettings.startingTor "Gaida, kamēr Tor startēs...">
<!ENTITY torsettings.restartTor "Restartēt Tor">
<!ENTITY torsettings.reconfigTor "Pārkonfigurēt">

<!ENTITY torsettings.discardSettings.prompt "Jūs nokonfigurējāt Tor tiltus vai ievadījāt vietējā starpniekservera iestatījumus. Lai izveidot tiešu savienojumu ar tīklu Tor, šie iestatījumi ir jānoņem.">
<!ENTITY torsettings.discardSettings.proceed "Noņemt Iestatījumus un Izveidot savienojumu ">

<!ENTITY torsettings.optional "Neobligāts">

<!ENTITY torsettings.useProxy.checkbox "Lai izveidotu savienojumu ar internetu, es lietoju starpniekseveri">
<!ENTITY torsettings.useProxy.type "Starpniekservera tips">
<!ENTITY torsettings.useProxy.type.placeholder "izvēlieties starpniekservera veidu">
<!ENTITY torsettings.useProxy.address "Adrese">
<!ENTITY torsettings.useProxy.address.placeholder "IP adrese vai resursdatora nosaukums">
<!ENTITY torsettings.useProxy.port "Ports">
<!ENTITY torsettings.useProxy.username "Lietotājvārds">
<!ENTITY torsettings.useProxy.password "Parole">
<!ENTITY torsettings.useProxy.type.socks4 "SOCKS 4">
<!ENTITY torsettings.useProxy.type.socks5 "SOCKS 5">
<!ENTITY torsettings.useProxy.type.http "HTTP / HTTPS">
<!ENTITY torsettings.firewall.checkbox "Šis dators iet caur ugunsmūri, kurš atļauj tikai savienojumus ar noteiktiem portiem.">
<!ENTITY torsettings.firewall.allowedPorts "Atļautie porti">
<!ENTITY torsettings.useBridges.checkbox "Manā valstī Tor tiek cenzēts">
<!ENTITY torsettings.useBridges.default "Izvēlieties iebūvētu tiltu">
<!ENTITY torsettings.useBridges.default.placeholder "izvēlieties tiltu">
<!ENTITY torsettings.useBridges.bridgeDB "Pieprasīt tiltu no torproject.org">
<!ENTITY torsettings.useBridges.captchaSolution.placeholder "Ievadīt burtus no attēla">
<!ENTITY torsettings.useBridges.reloadCaptcha.tooltip "Ielikt jaunu izaicinājumu">
<!ENTITY torsettings.useBridges.captchaSubmit "Iesniegt">
<!ENTITY torsettings.useBridges.custom "Piedāvāt tiltu, kuru es zinu">
<!ENTITY torsettings.useBridges.label "Ievadiet uzticama avota sniegtu tilta informāciju.">
<!ENTITY torsettings.useBridges.placeholder "rakstiet address:port (vienu rindā)">

<!ENTITY torsettings.copyLog "Kopēt Tor žurnālu starpliktuvē">

<!ENTITY torsettings.proxyHelpTitle "Starpniekservera palīdzība">
<!ENTITY torsettings.proxyHelp1 "A local proxy might be needed when connecting through a company, school, or university network.&#160;If you are not sure whether a proxy is needed, look at the Internet settings in another browser or check your system's network settings.">

<!ENTITY torsettings.bridgeHelpTitle "Palīdzība par tiltu retranslatoriem">
<!ENTITY torsettings.bridgeHelp1 "Tilti ir neuzskaitīti pārraidītāji, kas padara sareģītāku savienojumu bloķēšanu ar tīklu Tor. Katrs tiltu veids lieto citu cenzūras apiešanas metodi. obfs jeb maskējošie padara Jūsu datu plūsmu līdzīgu nejaušam troksnim, bet meek jeb pielaidīgie padara Jūsu datu plūsmu līdzīgu tādai, kas pievienojas attiecīgajam pkalpojumam nevis Tor.">
<!ENTITY torsettings.bridgeHelp2 "Tā kā dažās valstīs cenšas atšķirīgi bloķēt tīklu Tor, daži tilti strādā vienās valstīs, bet nestrādā citās. Ja neesat pārliecināts, par to, kuri tilti strādā Jūsu valstī, skatiet torproject.org/about/contact.html#support">

<!-- Progress -->
<!ENTITY torprogress.pleaseWait "Lūdzu pagaidiet kamēr mēs izveidojam savienojumu ar tīklu Tor. Tam var būt nepieciešamas vairākas minūtes.">

<!-- #31286 about:preferences strings -->
<!ENTITY torPreferences.categoryTitle "Savienojums">
<!ENTITY torPreferences.torSettings "Tor iestatījumi">
<!ENTITY torPreferences.torSettingsDescription "Tor Browser routes your traffic over the Tor Network, run by thousands of volunteers around the world." >
<!ENTITY torPreferences.learnMore "Uzzināt vairāk">
<!-- Status -->
<!ENTITY torPreferences.statusInternetLabel "Internets:">
<!ENTITY torPreferences.statusInternetTest "Pārbaude">
<!ENTITY torPreferences.statusInternetOnline "Tiešsaiste">
<!ENTITY torPreferences.statusInternetOffline "Bezsaistē">
<!ENTITY torPreferences.statusTorLabel "Tīkls Tor:">
<!ENTITY torPreferences.statusTorConnected "Savienots">
<!ENTITY torPreferences.statusTorNotConnected "Nav savienots">
<!ENTITY torPreferences.statusTorBlocked "Potenciāli bloķēts">
<!ENTITY torPreferences.learnMore "Uzzināt vairāk">
<!-- Quickstart -->
<!ENTITY torPreferences.quickstart "Quickstart">
<!ENTITY torPreferences.quickstartDescriptionLong "Quickstart connects Tor Browser to the Tor Network automatically when launched, based on your last used connection settings.">
<!ENTITY torPreferences.quickstartCheckbox "Vienmēr veidot savienojumu automātiski">
<!-- Bridge settings -->
<!ENTITY torPreferences.bridges "Tilti">
<!ENTITY torPreferences.bridgesDescription "Bridges help you access the Tor Network in places where Tor is blocked. Depending on where you are, one bridge may work better than another.">
<!ENTITY torPreferences.bridgeLocation "Jūsu atrašanās vieta">
<!ENTITY torPreferences.bridgeLocationAutomatic "Automātiski">
<!ENTITY torPreferences.bridgeLocationFrequent "Bieži izvēlētas atrašanās vietas">
<!ENTITY torPreferences.bridgeLocationOther "Citas atrašanās vietas">
<!ENTITY torPreferences.bridgeChooseForMe "Izvēlieties tiltu man!">
<!ENTITY torPreferences.bridgeBadgeCurrent "Jūsu esošie tilti">
<!ENTITY torPreferences.bridgeBadgeCurrentDescription "You can save one or more bridges, and Tor will choose which one to use when you connect. Tor will automatically switch to use another bridge when needed.">
<!ENTITY torPreferences.bridgeId "#1 tilts: #2"> <!-- #1 = bridge type; #2 = bridge emoji id -->
<!ENTITY torPreferences.remove "Noņemt">
<!ENTITY torPreferences.bridgeDisableBuiltIn "Atspējot iebūvētus tiltus">
<!ENTITY torPreferences.bridgeShare "Share this bridge using the QR code or by copying its address:">
<!ENTITY torPreferences.bridgeCopy "Kopēt tilta adresi">
<!ENTITY torPreferences.copied "Nokopēts">
<!ENTITY torPreferences.bridgeShowAll "Rādīt visus tiltus">
<!ENTITY torPreferences.bridgeRemoveAll "Noņemt visus tiltus">
<!ENTITY torPreferences.bridgeAdd "Pievienot jaunu tiltu">
<!ENTITY torPreferences.bridgeSelectBrowserBuiltin "Izvēlēies kādu no pārlūka Tor iebūvētajiem tiltiem">
<!ENTITY torPreferences.bridgeSelectBuiltin "Izvēlēties iebūvētu tiltu...">
<!ENTITY torPreferences.bridgeRequest "Pieprasīt tiltu...">
<!ENTITY torPreferences.bridgeEnterKnown "Ievadīt Jums jau zināma tilta adresi">
<!ENTITY torPreferences.bridgeAddManually "Pievienot tiltu manuāli...">
<!-- Advanced settings -->
<!ENTITY torPreferences.advanced "Lietpratīgs">
<!ENTITY torPreferences.advancedDescription "Configure how Tor Browser connects to the internet">
<!ENTITY torPreferences.advancedButton "Iestatījumi...">
<!ENTITY torPreferences.viewTorLogs "Skatīt Tor žurnālus">
<!ENTITY torPreferences.viewLogs "Skatīt žurnālus…">
<!-- Remove all bridges dialog -->
<!ENTITY torPreferences.removeBridgesQuestion "Noņemt visus tiltus?">
<!ENTITY torPreferences.removeBridgesWarning "Darbību nevar atsaukt.">
<!ENTITY torPreferences.cancel "Atcelt">
<!-- Scan bridge QR dialog -->
<!ENTITY torPreferences.scanQrTitle "Skenēt kvadrātkodu">
<!-- Builtin bridges dialog -->
<!ENTITY torPreferences.builtinBridgeTitle "Iebūvēti tilti">
<!ENTITY torPreferences.builtinBridgeDescription "Tor Browser includes some specific types of bridges known as “pluggable transports”.">
<!ENTITY torPreferences.builtinBridgeObfs4 "obfs4">
<!ENTITY torPreferences.builtinBridgeObfs4Description "obfs4 is a type of built-in bridge that makes your Tor traffic look random. They are also less likely to be blocked than their predecessors, obfs3 bridges.">
<!ENTITY torPreferences.builtinBridgeSnowflake "Snowflake">
<!ENTITY torPreferences.builtinBridgeSnowflakeDescription "Snowflake is a built-in bridge that defeats censorship by routing your connection through Snowflake proxies, ran by volunteers.">
<!ENTITY torPreferences.builtinBridgeMeekAzure "meek-azure">
<!ENTITY torPreferences.builtinBridgeMeekAzureDescription "meek-azure is a built-in bridge that makes it look like you are using a Microsoft web site instead of using Tor.">
<!-- Request bridges dialog -->
<!ENTITY torPreferences.requestBridgeDialogTitle "Pieprasīt tiltu">
<!ENTITY torPreferences.requestBridgeDialogWaitPrompt "Contacting BridgeDB. Please Wait.">
<!ENTITY torPreferences.requestBridgeDialogSolvePrompt "Solve the CAPTCHA to request a bridge.">
<!ENTITY torPreferences.requestBridgeErrorBadSolution "The solution is not correct. Please try again.">
<!-- Provide bridge dialog -->
<!ENTITY torPreferences.provideBridgeTitle "Provide Bridge">
<!ENTITY torPreferences.provideBridgeHeader "Enter bridge information from a trusted source">
<!-- Connection settings dialog -->
<!ENTITY torPreferences.connectionSettingsDialogTitle "Savienojuma iestatījumi">
<!ENTITY torPreferences.connectionSettingsDialogHeader "Configure how Tor Browser connects to the Internet">
<!ENTITY torPreferences.firewallPortsPlaceholder "Ar komatu atdalītas vērtības">
<!-- Log dialog -->
<!ENTITY torPreferences.torLogsDialogTitle "Tor žurnāli">

<!-- #24746 about:torconnect strings -->
<!ENTITY torConnect.notConnectedConcise "Nav savienots">
<!ENTITY torConnect.connectingConcise "Connecting…">
<!ENTITY torConnect.tryingAgain "Trying again…">
<!ENTITY torConnect.noInternet "Tor Browser couldn’t reach the Internet">
<!ENTITY torConnect.couldNotConnect "Tor Browser could not connect to Tor">
<!ENTITY torConnect.assistDescriptionConfigure "configure your connection"> <!-- used as a text to insert as a link on several strings (#1) -->
<!ENTITY torConnect.assistDescription "If Tor is blocked in your location, trying a bridge may help. Connection assist can choose one for you using your location, or you can #1 manually instead."> <!-- #1 = "configure your connection" link -->
<!ENTITY torConnect.tryingBridge "Trying a bridge…">
<!ENTITY torConnect.tryingBridgeAgain "Trying one more time…">
<!ENTITY torConnect.errorLocation "Tor Browser couldn’t locate you">
<!ENTITY torConnect.errorLocationDescription "Tor Browser needs to know your location in order to choose the right bridge for you. If you’d rather not share your location, #1 manually instead."> <!-- #1 = "configure your connection" link -->
<!ENTITY torConnect.isLocationCorrect "Are these location settings correct?">
<!ENTITY torConnect.isLocationCorrectDescription "Tor Browser still couldn’t connect to Tor. Please check your location settings are correct and try again, or #1 instead."> <!-- #1 = "configure your connection" link -->
<!ENTITY torConnect.breadcrumbAssist "Connection assist">
<!ENTITY torConnect.breadcrumbLocation "Location settings">
<!ENTITY torConnect.breadcrumbTryBridge "Try a bridge">
<!ENTITY torConnect.automatic "Automātiski">
<!ENTITY torConnect.selectCountryRegion "Select Country or Region">
<!ENTITY torConnect.frequentLocations "Bieži izvēlētas atrašanās vietas">
<!ENTITY torConnect.otherLocations "Citas atrašanās vietas">
<!ENTITY torConnect.restartTorBrowser "Pārstartēt pārlūku Tor">
<!ENTITY torConnect.configureConnection "Configure Connection…">
<!ENTITY torConnect.viewLog "View logs…">
<!ENTITY torConnect.tryAgain "Mēģiniet vēlreiz">
<!ENTITY torConnect.offline "Internet not reachable">
<!ENTITY torConnect.connectMessage "Changes to Tor Settings will not take effect until you connect">
<!ENTITY torConnect.tryAgainMessage "Tor Browser has failed to establish a connection to the Tor Network">
<!ENTITY torConnect.yourLocation "Jūsu atrašanās vieta">
<!ENTITY torConnect.tryBridge "Try a Bridge">
<!ENTITY torConnect.autoBootstrappingFailed "Automatic configuration failed">
<!ENTITY torConnect.autoBootstrappingFailed "Automatic configuration failed">
<!ENTITY torConnect.cannotDetermineCountry "Unable to determine user country">
<!ENTITY torConnect.noSettingsForCountry "No settings available for your location">
