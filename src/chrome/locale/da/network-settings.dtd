<!ENTITY torsettings.dialog.title "Tor-netværksindstillinger">
<!ENTITY torsettings.wizard.title.default "Opret forbindelse til Tor">
<!ENTITY torsettings.wizard.title.configure "Tor-netværksindstillinger">
<!ENTITY torsettings.wizard.title.connecting "Etablerer en forbindelse">

<!-- For locale picker: -->
<!ENTITY torlauncher.localePicker.title "Sprog for Tor Browser">
<!ENTITY torlauncher.localePicker.prompt "Vælg venligst et sprog.">

<!-- For "first run" wizard: -->

<!ENTITY torSettings.connectPrompt "Klik på “Opret forbindelse” for at oprette forbindelse til Tor.">
<!ENTITY torSettings.configurePrompt "Klik på “Konfigurer” for at justere netværksindstillinger hvis du er i et land som censurerer Tor (såsom Egypten, Kina, Tyrkiet) eller hvis du opretter forbindelse fra et privat netværk som kræver en proxy.">
<!ENTITY torSettings.configure "Konfigurer">
<!ENTITY torSettings.connect "Opret forbindelse">

<!-- Other: -->

<!ENTITY torsettings.startingTor "Venter på at Tor starter...">
<!ENTITY torsettings.restartTor "Genstart Tor">
<!ENTITY torsettings.reconfigTor "Genkonfigurer">

<!ENTITY torsettings.discardSettings.prompt "Du har konfigureret Tor-broer eller du har indtastet lokale proxyindstillinger.&#160; For at kunne foretage direkte forbindelse til Tor-netværket, skal indstillingerne fjernes.">
<!ENTITY torsettings.discardSettings.proceed "Fjern indstillinger og opret forbindelse">

<!ENTITY torsettings.optional "Valgfri">

<!ENTITY torsettings.useProxy.checkbox "Jeg bruger en proxy til at oprette forbindelse til internettet">
<!ENTITY torsettings.useProxy.type "Proxytype">
<!ENTITY torsettings.useProxy.type.placeholder "vælg en proxytype">
<!ENTITY torsettings.useProxy.address "Adresse">
<!ENTITY torsettings.useProxy.address.placeholder "IP-adresse eller værtsnavn">
<!ENTITY torsettings.useProxy.port "Port">
<!ENTITY torsettings.useProxy.username "Brugernavn">
<!ENTITY torsettings.useProxy.password "Adgangskode">
<!ENTITY torsettings.useProxy.type.socks4 "SOCKS 4">
<!ENTITY torsettings.useProxy.type.socks5 "SOCKS 5">
<!ENTITY torsettings.useProxy.type.http "HTTP/HTTPS">
<!ENTITY torsettings.firewall.checkbox "Computeren går gennem en firewall som kun tillader forbindelser til bestemte porte">
<!ENTITY torsettings.firewall.allowedPorts "Tilladte porte">
<!ENTITY torsettings.useBridges.checkbox "Tor er censureret i mit land">
<!ENTITY torsettings.useBridges.default "Vælg en indbygget bro">
<!ENTITY torsettings.useBridges.default.placeholder "vælg en bro">
<!ENTITY torsettings.useBridges.bridgeDB "Anmod om en bro fra torproject.org">
<!ENTITY torsettings.useBridges.captchaSolution.placeholder "Indtast tegnene fra billedet">
<!ENTITY torsettings.useBridges.reloadCaptcha.tooltip "Få en ny udfordring">
<!ENTITY torsettings.useBridges.captchaSubmit "Indsend">
<!ENTITY torsettings.useBridges.custom "Formidl en bro jeg kender">
<!ENTITY torsettings.useBridges.label "Indtast broinformation fra en betroet kilde.">
<!ENTITY torsettings.useBridges.placeholder "skriv adresse:port (én pr. linje)">

<!ENTITY torsettings.copyLog "Kopiér Tor-loggen til udklipsholderen">

<!ENTITY torsettings.proxyHelpTitle "Hjælp til proxy">
<!ENTITY torsettings.proxyHelp1 "Der kan være brug for en lokal proxy når der oprettes forbindelse gennem et netværk i virksomhed, skole eller universitet.&#160;Hvis du er i tvivl om der er brug for en proxy, så kig i internetindstillingerne i en anden browser eller tjek dit systems netværksindstillinger.">

<!ENTITY torsettings.bridgeHelpTitle "Hjælp til bro-relæ">
<!ENTITY torsettings.bridgeHelp1 "Broer er ulistede relæer som gør det sværrer at blokere forbindelser til Tor-netværket.&#160; Hver type bro bruger en anderledes metode for at undgå censur.&#160; Dem med obfs får din netværkstrafik til at ligne tilfældig støj og dem med meek får din netværkstrafik til at se ud som om den er forbundet til tjenesten i stedet for Tor.">
<!ENTITY torsettings.bridgeHelp2 "Pga. den måde bestemte lande prøver at blokere Tor, vil bestemte broer virke i nogle lande men ikke andre.&#160; Besøg torproject.org/about/contact.html#support hvis du er i tvivl om hvilke broer der virker i dit land.">

<!-- Progress -->
<!ENTITY torprogress.pleaseWait "Vent venligst mens der etableres forbindelse til Tor-netværket.&#160; Det kan tage flere minutter.">

<!-- #31286 about:preferences strings -->
<!ENTITY torPreferences.categoryTitle "Forbindelse">
<!ENTITY torPreferences.torSettings "Tor-indstillinger">
<!ENTITY torPreferences.torSettingsDescription "Tor Browser sender din trafik over Tor-netværket som køres af tusindvis af frivillige verden over." >
<!ENTITY torPreferences.learnMore "Lær mere">
<!-- Status -->
<!ENTITY torPreferences.statusInternetLabel "Internet:">
<!ENTITY torPreferences.statusInternetTest "Test">
<!ENTITY torPreferences.statusInternetOnline "Online">
<!ENTITY torPreferences.statusInternetOffline "Offline">
<!ENTITY torPreferences.statusTorLabel "Tor Network:">
<!ENTITY torPreferences.statusTorConnected "Forbundet">
<!ENTITY torPreferences.statusTorNotConnected "Ikke forbundet">
<!ENTITY torPreferences.statusTorBlocked "Potentially Blocked">
<!ENTITY torPreferences.learnMore "Lær mere">
<!-- Quickstart -->
<!ENTITY torPreferences.quickstart "Hurtig start">
<!ENTITY torPreferences.quickstartDescriptionLong "Quickstart connects Tor Browser to the Tor Network automatically when launched, based on your last used connection settings.">
<!ENTITY torPreferences.quickstartCheckbox "Opret altid forbindelse automatisk">
<!-- Bridge settings -->
<!ENTITY torPreferences.bridges "Broer">
<!ENTITY torPreferences.bridgesDescription "Broer hjælper dig med at tilgå Tor-netværket steder hvor Tor er blokeret. Afhængig af hvor du er, så kan en bro virke bedre end en anden.">
<!ENTITY torPreferences.bridgeLocation "Your location">
<!ENTITY torPreferences.bridgeLocationAutomatic "Automatisk">
<!ENTITY torPreferences.bridgeLocationFrequent "Frequently selected locations">
<!ENTITY torPreferences.bridgeLocationOther "Other locations">
<!ENTITY torPreferences.bridgeChooseForMe "Choose a Bridge For Me…">
<!ENTITY torPreferences.bridgeBadgeCurrent "Your Current Bridges">
<!ENTITY torPreferences.bridgeBadgeCurrentDescription "You can keep one or more bridges saved, and Tor will choose which one to use when you connect. Tor will automatically switch to use another bridge when needed.">
<!ENTITY torPreferences.bridgeId "#1 bridge: #2"> <!-- #1 = bridge type; #2 = bridge emoji id -->
<!ENTITY torPreferences.remove "Fjern">
<!ENTITY torPreferences.bridgeDisableBuiltIn "Disable built-in bridges">
<!ENTITY torPreferences.bridgeShare "Share this bridge using the QR code or by copying its address:">
<!ENTITY torPreferences.bridgeCopy "Copy Bridge Address">
<!ENTITY torPreferences.copied "Kopieret!">
<!ENTITY torPreferences.bridgeShowAll "Show All Bridges">
<!ENTITY torPreferences.bridgeRemoveAll "Remove All Bridges">
<!ENTITY torPreferences.bridgeAdd "Add a New Bridge">
<!ENTITY torPreferences.bridgeSelectBrowserBuiltin "Choose from one of Tor Browser’s built-in bridges">
<!ENTITY torPreferences.bridgeSelectBuiltin "Select a Built-In Bridge…">
<!ENTITY torPreferences.bridgeRequest "Anmod om en bro…">
<!ENTITY torPreferences.bridgeEnterKnown "Enter a bridge address you already know">
<!ENTITY torPreferences.bridgeAddManually "Add a Bridge Manually…">
<!-- Advanced settings -->
<!ENTITY torPreferences.advanced "Avanceret">
<!ENTITY torPreferences.advancedDescription "Configure how Tor Browser connects to the internet">
<!ENTITY torPreferences.advancedButton "Settings…">
<!ENTITY torPreferences.viewTorLogs "View the Tor logs">
<!ENTITY torPreferences.viewLogs "Vis logge…">
<!-- Remove all bridges dialog -->
<!ENTITY torPreferences.removeBridgesQuestion "Remove all the bridges?">
<!ENTITY torPreferences.removeBridgesWarning "This action cannot be undone.">
<!ENTITY torPreferences.cancel "Annuller">
<!-- Scan bridge QR dialog -->
<!ENTITY torPreferences.scanQrTitle "Scan the QR code">
<!-- Builtin bridges dialog -->
<!ENTITY torPreferences.builtinBridgeTitle "Built-In Bridges">
<!ENTITY torPreferences.builtinBridgeDescription "Tor Browser includes some specific types of bridges known as “pluggable transports”.">
<!ENTITY torPreferences.builtinBridgeObfs4 "obfs4">
<!ENTITY torPreferences.builtinBridgeObfs4Description "obfs4 is a type of built-in bridge that makes your Tor traffic look random. They are also less likely to be blocked than their predecessors, obfs3 bridges.">
<!ENTITY torPreferences.builtinBridgeSnowflake "Snowflake">
<!ENTITY torPreferences.builtinBridgeSnowflakeDescription "Snowflake is a built-in bridge that defeats censorship by routing your connection through Snowflake proxies, ran by volunteers.">
<!ENTITY torPreferences.builtinBridgeMeekAzure "meek-azure">
<!ENTITY torPreferences.builtinBridgeMeekAzureDescription "meek-azure is a built-in bridge that makes it look like you are using a Microsoft web site instead of using Tor.">
<!-- Request bridges dialog -->
<!ENTITY torPreferences.requestBridgeDialogTitle "Anmod om bro">
<!ENTITY torPreferences.requestBridgeDialogWaitPrompt "Kontakter BridgeDB. Vent venligst.">
<!ENTITY torPreferences.requestBridgeDialogSolvePrompt "Løs CAPTCHA'en for at anmode om en bro.">
<!ENTITY torPreferences.requestBridgeErrorBadSolution "Løsningen er ikke korrekt. Prøv venligst igen.">
<!-- Provide bridge dialog -->
<!ENTITY torPreferences.provideBridgeTitle "Provide Bridge">
<!ENTITY torPreferences.provideBridgeHeader "Indtast broinformation fra en betroet kilde">
<!-- Connection settings dialog -->
<!ENTITY torPreferences.connectionSettingsDialogTitle "Connection Settings">
<!ENTITY torPreferences.connectionSettingsDialogHeader "Configure how Tor Browser connects to the Internet">
<!ENTITY torPreferences.firewallPortsPlaceholder "Comma-seperated values">
<!-- Log dialog -->
<!ENTITY torPreferences.torLogsDialogTitle "Tor-logge">

<!-- #24746 about:torconnect strings -->
<!ENTITY torConnect.notConnectedConcise "Ikke forbundet">
<!ENTITY torConnect.connectingConcise "Forbinder...">
<!ENTITY torConnect.tryingAgain "Trying again…">
<!ENTITY torConnect.noInternet "Tor Browser couldn’t reach the Internet">
<!ENTITY torConnect.couldNotConnect "Tor Browser could not connect to Tor">
<!ENTITY torConnect.assistDescriptionConfigure "configure your connection"> <!-- used as a text to insert as a link on several strings (#1) -->
<!ENTITY torConnect.assistDescription "If Tor is blocked in your location, trying a bridge may help. Connection assist can choose one for you using your location, or you can #1 manually instead."> <!-- #1 = "configure your connection" link -->
<!ENTITY torConnect.tryingBridge "Trying a bridge…">
<!ENTITY torConnect.tryingBridgeAgain "Trying one more time…">
<!ENTITY torConnect.errorLocation "Tor Browser couldn’t locate you">
<!ENTITY torConnect.errorLocationDescription "Tor Browser needs to know your location in order to choose the right bridge for you. If you’d rather not share your location, #1 manually instead."> <!-- #1 = "configure your connection" link -->
<!ENTITY torConnect.isLocationCorrect "Are these location settings correct?">
<!ENTITY torConnect.isLocationCorrectDescription "Tor Browser still couldn’t connect to Tor. Please check your location settings are correct and try again, or #1 instead."> <!-- #1 = "configure your connection" link -->
<!ENTITY torConnect.breadcrumbAssist "Connection assist">
<!ENTITY torConnect.breadcrumbLocation "Location settings">
<!ENTITY torConnect.breadcrumbTryBridge "Try a bridge">
<!ENTITY torConnect.automatic "Automatisk">
<!ENTITY torConnect.selectCountryRegion "Select Country or Region">
<!ENTITY torConnect.frequentLocations "Frequently selected locations">
<!ENTITY torConnect.otherLocations "Other locations">
<!ENTITY torConnect.restartTorBrowser "Restart Tor Browser">
<!ENTITY torConnect.configureConnection "Configure Connection…">
<!ENTITY torConnect.viewLog "View logs…">
<!ENTITY torConnect.tryAgain "Try Again">
<!ENTITY torConnect.offline "Internet not reachable">
<!ENTITY torConnect.connectMessage "Changes to Tor Settings will not take effect until you connect">
<!ENTITY torConnect.tryAgainMessage "Tor Browser kunne ikke etablere en forbindelse til Tor-netværket">
<!ENTITY torConnect.yourLocation "Your Location">
<!ENTITY torConnect.tryBridge "Try a Bridge">
<!ENTITY torConnect.autoBootstrappingFailed "Automatic configuration failed">
<!ENTITY torConnect.autoBootstrappingFailed "Automatic configuration failed">
<!ENTITY torConnect.cannotDetermineCountry "Unable to determine user country">
<!ENTITY torConnect.noSettingsForCountry "No settings available for your location">
